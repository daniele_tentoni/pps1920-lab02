package u02

object Fibonacci {
  /**
   * Fibonacci function with recursion.
   * @param x Number of fibonacci number to return.
   * @return The x number of fibonacci.
   */
  def fib(x: Int): Int = x match {
    case 0 | 1 => x
    case x if x > 0 => fib(x - 1) + fib(x - 2)
    case _ => 0
  }
}

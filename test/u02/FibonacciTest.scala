package u02

import org.junit._
import org.junit.Assert._

class FibonacciTest {

  import Fibonacci._
  @Test def testFib0(): Unit = {
    assertEquals(0, fib(0))
  }

  @Test def testFib1() {
    assertEquals(1, fib(1))
  }

  @Test def testFib2() {
    assertEquals(1, fib(2))
  }

  @Test def testFib3to5() {
    assertEquals(2, fib(3))
    assertEquals(3, fib(4))
    assertEquals(5, fib(5))
  }

  @Test def testFib6to9() {
    assertEquals(8, fib(6))
    assertEquals(13, fib(7))
    assertEquals(21, fib(8))
  }
}

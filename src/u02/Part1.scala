package u02

object Part1 extends App {
  val parity2: Int => String = {
    case x if x % 2 == 0 => "even"
    case _ => "odd"
  }

  def parity1(x:Int): String = x match {
    case _ % 2 == 0 => "even"
    case _ => "odd"
  }

  println(parity1(1))
  println(parity1(2))
  println(parity2(3))
  println(parity2(4))
}

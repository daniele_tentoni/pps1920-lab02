package u02

import org.junit.Test
import org.junit.Assert._

class ParityTest {

  import Part1._
  @Test def testParity1() {
    assertEquals("even", parity1(2))
    assertEquals("odd", parity2(5))
  }
}

package u02

object Part2 extends App {
  object Part {
    val empty: String => Boolean = _ == ""

    val neg: (String => Boolean) =>
      String => Boolean =
      p => s => !p(s)
  }
}

package u02

object ShapeC extends App {

  sealed trait Shape[A, B]

  object ShapeImpl {

    val pi = 3.1415

    case class Rectangle[A, B](b: A, h: B) extends Shape[A, B]
    case class Circle[A, B](d: A) extends Shape[A, B]
    case class Square[A, B](b: A) extends Shape[A, B]

    def perimeter[A, B](shape: Shape[A, B]): Double =
      shape match {
        case Circle(d: Double) => d * pi
        case Square(b: Double) => b * 4.0
        case Rectangle(b: Double, h: Double) => (b + h) * 2.0
        case _ => 0
      }

    def area[A, B](shape: Shape[A, B]): Double =
      shape match {
        case Circle(d: Double) => (d / 2.0) * (d / 2.0) * pi
        case Square(d: Double) => d * d
        case Rectangle(b: Double, h: Double) => b * h
        case _ => 0
      }
  }

}
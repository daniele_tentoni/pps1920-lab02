package u02

import org.junit._
import org.junit.Assert._
import u02.ShapeC.ShapeImpl._

class ShapeTest {

  @Test def testRectangle(): Unit ={
    val rect = Rectangle(2, 4)
    assertEquals(2, rect.b, 0.1)
    assertEquals(4, rect.h, 0.1)
  }

  @Test def testCircle(): Unit ={
    val circle = Circle(2)
    assertEquals(2, circle.d, 0.1)
  }

  @Test def testSquare(): Unit ={
    val square = Square(2)
    assertEquals(2, square.b, 0.1)
  }

  @Test def testRectangleFunctions(): Unit = {
    val rectangle = Rectangle(4, 6)
    assertEquals(20, perimeter(rectangle), 1.0)
    assertEquals(24, area(rectangle), 1.0)
  }

  @Test def testCircleFunctions(): Unit = {
    val circle = Circle(5)
    assertEquals(15.7, perimeter(circle), 1.0)
    assertEquals(19.625, area(circle), 1.0)
  }

  @Test def testSquareFunctions(): Unit = {
    val square = Square(5)
    assertEquals(20, perimeter(square), 1.0)
    assertEquals(25, area(square), 1.0)
  }
}

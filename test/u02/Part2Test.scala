package u02

import org.junit.Test
import org.junit.Assert._

class Part2Test {

  import Part2.Part._
  @Test def TestEmpty(): Unit = {
    assertTrue(empty(""))
    assertFalse(empty("1"))
  }

  @Test def TestLambda(): Unit = {
    val s: String => Boolean = _ == ""
    assertFalse(s("1"))
    assertTrue(s(""))
  }

  @Test def TestNotEmpty(): Unit = {
    val notEmpty: String => Boolean =
      neg(empty)
    assertFalse(notEmpty(""))
    assertTrue(notEmpty("1"))
  }

  @Test def testNegLambda(): Unit = {
    val notLambda: String => Boolean =
      neg(_ == "")
    assertFalse(notLambda(""))
    assertTrue(notLambda("1"))
  }
}
